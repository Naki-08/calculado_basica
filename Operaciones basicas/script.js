function realizarOperaciones() {
    const num1 = parseFloat(document.getElementById('num1').value);
    const num2 = parseFloat(document.getElementById('num2').value);
    const table = document.getElementById('resultado');

    // Limpiar la tabla de resultados
    table.innerHTML = '<tr><th>Iteración</th><th>Operación</th><th>Resultado</th></tr>';

    // Realizar las operaciones en un bucle de 5 iteraciones
    for (let i = 1; i <= 5; i++) {
        let resultado;
        let operacion;

        switch (i) {
            case 1:
                resultado = num1 + num2;
                operacion = 'SUMA';
                break;
            case 2:
                resultado = num1 - num2;
                operacion = 'RESTA';
                break;
            case 3:
                resultado = num1 * num2;
                operacion = 'MULTIPLICACIÓN';
                break;
            case 4:
                if (num2 !== 0) {
                    resultado = num1 / num2;
                    operacion = 'DIVISIÓN';
                } else {
                    resultado = 'No se puede dividir por 0';
                    operacion = 'DIVISIÓN';
                }
                break;
            case 5:
                resultado = num1 % num2;
                operacion = 'MOD (%)';
                break;
        }

        // Agregar el resultado a la tabla
        const row = `<tr><td>${i}</td><td>${operacion}</td><td>${resultado}</td></tr>`;
        table.innerHTML += row;
    }
}
